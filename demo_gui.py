#!/usr/bin/python3
#07-24-2019
#Test and demonstrate the usage of the multi-purpose graphing GUI
#------------------------------------------------------------------------------
from main_thread import ProcMaster
from sn_GUI4 import NodeGUI
import threading
import time
import random
from comm_queue import Queue

#Create the ProcMaster event handler singleton
mainT = ProcMaster()

NUM_BARS = 4
NUM_LINES = 3

#Create the NodeGui singleton
NodeGUI();

#Init packet format :   type|BAR_FORMAT::LINE_FORMAT
#BAR FORMAT         :   name,init_val_INT
#LINE FORMAT        :   name,minX,maxX.minY,maxY,SCROLLABLE_INT,BUFFER_INT
gui_4_init_packet = "bar0,0:bar1,5:bar2,25:bar3,90";
gui_4_init_packet += "::";
gui_4_init_packet += "line0,0,99,0,99,0,0:";
gui_4_init_packet += "line1,0,99,0,99,1,0:";
gui_4_init_packet += "line2,0,99,0,99,1,0";

mainT.event['buildup'].post(gui_4_init_packet);

#Demonstrate functionality

print("Demonstrating functionality...");
print("----------Bar Graphs-----------");
print("Incrementing bar0 by 95");
packet = ('bar:0:95') 
mainT.event['process'].post(packet);
NodeGUI().getWindow().Read(timeout=1000);

print("Incrementing bar1 by 90");
packet = ('bar:1:90') 
mainT.event['process'].post(packet);
NodeGUI().getWindow().Read(timeout=1000);

print("Incrementing bar2 by 70");
packet = ('bar:2:70') 
mainT.event['process'].post(packet);
NodeGUI().getWindow().Read(timeout=1000);

print("Incrementing bar3 by 5");
packet = ('bar:3:5') 
mainT.event['process'].post(packet);
NodeGUI().getWindow().Read(timeout=1000);
print("---------Line Graphs-----------");
x=0;
while x < 100:
    p1='line:0:%d,%d' % (x,x*x);
    mainT.event['process'].post(p1);
    p2='line:1:%d,%d' % (x,x*x);
    mainT.event['process'].post(p2);
    p3='line:2:%d,%d' % (x,x*x);
    mainT.event['process'].post(p3);
    NodeGUI().getWindow().Read(timeout=1000);
    x+=1;
print("-------------------------------");

#Send random data to the GUI until the gui is closed
x = 0;
while(1):
    #Generate data packet
    if(not random.randint(0,1)):
        packet = ('bar:%d:%d' % (random.randint(0,NUM_BARS-1), random.randint(1,5)));
    else:
        packet = ('line:%d:%d,%d' % (random.randint(0,NUM_LINES-1), x, x*x));
        x += 1;

    #Update the gui by posting data for the graph elements to process
    mainT.event['process'].post(packet);

    #Update the window in a non-blocking fashion
    #If the timeout event is received from the window (the window is closed),
    #   then post to teardown and terminate
    a,b = NodeGUI().getWindow().Read(timeout=500);
    if(str(a) != '__TIMEOUT__'):
        mainT.event['teardown'].post();
        exit(0);

