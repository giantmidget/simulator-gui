#!/usr/bin/python3
#------------------------------------------------------------------------------
#7-17-2019
#Class for the line graph represented by the PYSimpleGUI.Graph element in the 
#NodeGUI class
#---
#Upon initalization, set the name, domain, range, and scrollable option
#if incorrect number of parameters is specified, an error is printed and the  
#instance is created with default values
#------------------------------------------------------------------------------
import random
import PySimpleGUI as gui

class LineGraph():
    
    NAME = None         #same as that of the PYSimpleGUI object
    GRAPH = None        #the instance of the PYSImpleGUI Graph object associated with this object
    WINDOW= None        #the window the graph is located in
    
    REAL_VALUES = []    #contains the values being received by the LineGraph
    PLOTTED_VALUES = [] #contains the points being sent to the NodeGui.Graph object
    SIZE = (99,99)      #the dimensions of the graph object in pixels
    GRAPH_XVALS = (0,SIZE[0])#the range of min-max x values used for creation of the graph object
    GRAPH_YVALS = (0,SIZE[1])#the range of min-max y values used for creation of the graph object
    XVALS = GRAPH_XVALS #the real range of x values to be used for graphing
    YVALS = GRAPH_YVALS #the real range of y values to be used for graphing
    SCROLLABLE = 1      #specifies graph's scrollability condition (0-none, 1=resize)
    SHIFT_BORDER = 0    #Padding which to add to the increase in graph dimensions if the graph is scrollable
    
    BG_COLOR = 'BLACK'  #color for the graph background
    LN_COLOR = 'WHITE'  #color for the graph elements
    
    #@param params[] : an array containing, in this order - (str)name of the graph object, (tuple) size of the object in pixels, 
    #                  (tuple) the range of x values for the graph, (tuple) the range of y values for the graph, (int) SCROLLABLE,
    #                  and (int) a pixel buffer to maintain with the graphs edge in the event the graph shifts
    #@param bg_color    : the color of the graph background
    #@param ln_color    : the color of graph elements
    def __init__(self, params, bg_color = 'BLACK', ln_color = 'WHITE', graph_pad = (0,0)):
        global GRAPH, WINDOW, REAL_VALUES, PLOTTED_VALUES, GRAPH_XVALS, GRAPH_YVALS, BG_COLOR, LN_COLOR
        global NAME, SIZE, XVALS, YVALS, SCROLLABLE, SHIFT_BORDER
        try:
            self.NAME = params[0]
            self.SIZE = params[1]
            self.XVALS = params[2]
            self.YVALS  = params[3]
            self.SCROLLABLE = params[4]
            self.SHIFT_BORDER = params[5]
        except:
            print("%s - Error initializing variables, creating w/ defaults..." % str(self))
            self.NAME = str("UNNAMED-" + str(random.randint(0,9999)))
            self.SIZE = (500,500)
            self.XVALS = (0,99)
            self.YVALS = (0,99)
            self.REAL_VALUES = PLOTTED_VALUES = []
            self.SCROLLABLE = 1
            self.SHIFT_BORDER = 0

        self.GRAPH_XVALS = (0,self.SIZE[0])
        self.GRAPH_YVALS = (0,self.SIZE[1])
        self.BG_COLOR = str(bg_color);
        self.LN_COLOR = str(ln_color);
        self.GRAPH = gui.Graph(self.SIZE, (self.GRAPH_XVALS[0], self.GRAPH_YVALS[0]), (self.GRAPH_XVALS[1], self.GRAPH_YVALS[1]), pad = graph_pad, background_color=self.BG_COLOR, key=self.NAME)
        self.PLOTTED_VALUES = []

    #Shifts the graph to encompass all points - invoked only when SCROLLABLE=1
    #
    def shiftGraph(self):
        global GRAPH, WINDOW, REAL_VALUES, PLOTTED_VALUES, GRAPH_XVALS, GRAPH_YVALS, XVALS, YVALS, SHIFT_BORDER, BG_COLOR, LN_COLOR, NAME
        
        
        finVal = self.REAL_VALUES[self.REAL_VALUES.__len__()-1]
        
        if(finVal[0] < self.XVALS[0]):
            self.XVALS = (finVal[0] - self.SHIFT_BORDER, self.XVALS[1])
        elif(finVal[0] > self.XVALS[1]):
            self.XVALS = (self.XVALS[0], finVal[0] + self.SHIFT_BORDER)

        if(finVal[1] < self.YVALS[0]):
            self.YVALS = (finVal[1] - self.SHIFT_BORDER, self.YVALS[1])
        elif(finVal[1] > self.YVALS[1]):
            self.YVALS = (self.YVALS[0], finVal[1] + self.SHIFT_BORDER)
            
        print("X-Range : " + str(self.XVALS) + ", Y-Range : " + str(self.YVALS))
        
        print(self.GRAPH.DrawRectangle((self.GRAPH_XVALS[0], self.GRAPH_YVALS[1]),(self.GRAPH_XVALS[1], self.GRAPH_YVALS[0]), fill_color=self.BG_COLOR, line_color=self.BG_COLOR))
        self.GRAPH.Erase()

        cnt = 0
        NEW_PLOT_VALUES = []
        for i in self.REAL_VALUES:
            i_x = int(((i[0]-self.XVALS[0]) / (self.XVALS[1]-self.XVALS[0])) * (self.GRAPH_XVALS[1] - self.GRAPH_XVALS[0]))
            i_y = int(((i[1]-self.YVALS[0]) / (self.YVALS[1]-self.YVALS[0])) * (self.GRAPH_YVALS[1] - self.GRAPH_YVALS[0]))
            NEW_PLOT_VALUES.append((i_x,i_y))

        self.PLOTTED_VALUES = NEW_PLOT_VALUES
        print(self.REAL_VALUES)
        print(self.PLOTTED_VALUES)

        for i in self.PLOTTED_VALUES:

            self.GRAPH.DrawPoint(i, size=2, color=self.LN_COLOR);
            if(self.PLOTTED_VALUES.__len__() >= 2 and cnt > 0):
                pass
                self.GRAPH.DrawLine(self.PLOTTED_VALUES[cnt-1], i, color=self.LN_COLOR, width=2)
            cnt+=1
            
        
    #Moves the graph window to contain the last added point - invoked only when SCROLLABLE=2
    # @param point the last added point
    #
    def moveGraph(self, point):
        pass
            
    #Adds a point to the line graph, and shifts the graph according to the SCROLLABLE global if required 
    # @param        point (tuple) the point to be added to the graph
    # @return       1 if the point passed is not a tuple
    #
    def addPoint(self, point):
        global GRAPH, REAL_VALUES, PLOTTED_VALUES, SCROLLABLE, LN_COLOR, SCROLLABLE, XVALS, YVALS

        if(not isinstance(point, tuple)): 
            return 1

        self.REAL_VALUES.append(point)

        if(((point[0] < self.XVALS[0]) or (point[0] > self.XVALS[1]) or (point[1] < self.YVALS[0]) or (point[1] > self.YVALS[1]))):
            if(self.SCROLLABLE == 1):
                self.shiftGraph()
                return [self.XVALS, self.YVALS]
            elif(self.SCROLLABLE == 2):
                self.moveGraph(point)
                return [self.XVALS, self.YVALS]
            
        i_x = int(((point[0]-self.XVALS[0]) / (self.XVALS[1]-self.XVALS[0])) * (self.GRAPH_XVALS[1] - self.GRAPH_XVALS[0]))
        i_y = int(((point[1]-self.YVALS[0]) / (self.YVALS[1]-self.YVALS[0])) * (self.GRAPH_YVALS[1] - self.GRAPH_YVALS[0]))
        self.PLOTTED_VALUES.append((i_x, i_y))
        self.GRAPH.DrawPoint(self.PLOTTED_VALUES[self.PLOTTED_VALUES.__len__()-1], size=2, color=self.LN_COLOR)
        if(self.PLOTTED_VALUES.__len__() >= 2):
            self.GRAPH.DrawLine(self.PLOTTED_VALUES[self.PLOTTED_VALUES.__len__() - 2], self.PLOTTED_VALUES[self.PLOTTED_VALUES.__len__()-1], color=self.LN_COLOR, width=2)
        
        return [self.XVALS, self.YVALS]
            
            
#!#    def setWindow(self, win): global WINDOW; WINDOW=win;
#!#    def getWindow(self): global WINDOW; return WINDOW;
    
    def setGraph(self, gr): global GRAPH; GRAPH=gr;
    def getGraph(self): global GRAPH; return self.GRAPH;
