#!/usr/bin/python3
#------------------------------------------------------------------------------
#07-01-2019
#----------
#GUI front end for the sensor node simulator (version 5) - incorporates event handler
#   -adds more modularity to the gui, removes 'tab' feature present for use with various nodes, which was present in version 3
#   -the process() method, invoked by the process event, updates the info in the gui elements by reading from the queue
#------------------------------------------------------------------------------
#---To generate the layout, specify the number of bar and line graphs in a post to 'buildup' using the format of the payload below
#    <(str)name_bar_0,(int)init_val>:...:<(str)name_bar_N,(int)init_val>::<(str)name_line_0,(int)minX,(int)maxX,(int)minY,(int)maxY,(int)SCROLLABLE,(int)BUFFER>:...:
#        -NOTE: in the above format, the quotes () and their contents are a reference as for what data types the values should be,
#               and are not included within the actual packet. See demo_gui.py for example packets.
#
#---To update the graphs, send the data in a post to 'process' in a packet of the below format
#<bar|line>:<graph_no>:<val|{x,y}> 
#    -graph_no is an index to the list bars[] or lines[], depending on the type of graph,
#    that holds data about the graphs. The index of a graph in this list is determined by
#    the order they are received in the buildup packet. (the first type of a graph specified 
#    would have index 0 in the list for the type of graph it is (bars[] or lines[])
#    -NOTE: see demo_gui.py for sample packets
#
#NOTE : at this time no input validation is performed on the payloads before packet data is processed - it is the responsibility of the 
#sender to ensure compliance with the above formatting.
#------------------------------------------------------------------------------

import PySimpleGUI as gui;
from comm_queue import Queue;
import threading;
import time;
from main_thread import ProcMaster;
from linegraph import LineGraph; 
#from nodes import NodeFactory;

class NodeGUI(object):

    __instance: object = None; 

    THREAD_KILL = 0;                    #set to 1 to kill the listener thread
    window = None;                      #the PYSimpleGUI Window object
    queue_data = None;                  #the Queue object to hold data posted through the process event

    bars = [];                          #holds the names and values of the arbitrary amount of bar graphs in the GRAPHING GUI, in the format [['name',value], ...]
    lines = [];                         #holds the names and values of the arbitrary amount of line graphs in the GRAPHING GUI, in the format [['name', [<values>]], ...]
    
    HEIGHT = 500;                       #height of the Window object
    WIDTH = 1500;                       #width of the Window object
    NUM_LINES = 1;                      #the number of line graphs in the Window object
    NUM_BARS = 1;                       #the number of bar graphs in the Window object
    LINES_HEIGHT = None;                #the height of line graph window elements
    LINES_WIDTH = None;                 #the width of line graph window elements
    BARS_HEIGHT = None;                 #the height of bar graph window elements
    BARS_WIDTH = None;                  #the width of bar graph window elements
    BG_COLOR = 'black';                 #background color of the window graph elements
    GRAPH_ELEMENT_COLOR = 'white';      #color of lines and bars drawn on the window graph elements
    FNT_SIZE = 8;                       #font size for text displayed in the window

    #Update the data displayed in the GUI, if the GUI type is 'graphing'
    # @param    data    (tuple) the data to be displayed in the graphing GUI
    # @return           (int) 1 if error and 0 if success
    #
    def updateGraphingGUI(self, data):
        print(data);
        if(data[0] == 'bar'):
            bars[int(data[1])][1] += int(data[2]);    
            gui.Graph.DrawRectangle(window.FindElement(bars[int(data[1])][0]), (0,bars[int(data[1])][1]), (BARS_WIDTH,0), fill_color=GRAPH_ELEMENT_COLOR)
            gui.Text.Update(window.FindElement(str(bars[int(data[1])][0]) + "_info"), str(bars[int(data[1])][1]));
        elif(data[0] == 'line'):
            new_dom_rng = lines[int(data[1])][1].addPoint((int(data[2].split(',')[0]), int(data[2].split(',')[1])));
            
            if new_dom_rng != 1:
                gui.Text.Update(window.FindElement(lines[int(data[1])][0] + "_DOM"), str('(' + str(new_dom_rng[0][0]) + ',' + str(new_dom_rng[0][1]) + ')'))
                gui.Text.Update(window.FindElement(lines[int(data[1])][0] + "_RNG"), str('(' + str(new_dom_rng[1][0]) + ',' + str(new_dom_rng[1][1]) + ')'))

            gui.Multiline.Update(window.FindElement(lines[int(data[1])][0] + "_tab"), str('(' + str(data[2].split(',')[0]) + ',' + str(data[2].split(',')[1]) + ')\n'), append=True);
        else:
            print("Malformed data " + str(data));
            return 1;
        return 0;

    #Method for the listener thread called in open()
    #    - loops until the THREAD_KILL boolean is set to 1 by the 'teardown' event
    #    - checks the queue every second while it is empty, if it isn't process the data until the queue is empty
    #    -sends the data to the updateGraphingGUI() method
    #
    def listen(self):
        global THREAD_KILL; 
        data = None;
        while(not THREAD_KILL):
            if(Queue.__len__(queue_data) != 0):
                data = str(Queue.get(queue_data, 1)).split(':');
                self.updateGraphingGUI(data);
            else:
                time.sleep(1);

    #Adds data to the queue - invoked by the event process
    # @param    packet    (str) the packet to be submitted to the queue
    #
    def update(packet):
        print(packet); 
        if(packet['dest'] == 'sn_GUI4' and packet['event'] == 'process'):
            Queue.put(queue_data, packet['payload']);

    #Returns Queue object
    # @return   (Queue) queue_data
    #
    def getQueue(self):
        global queue_data
        return queue_data

    #Initialize the NodeGUI - if initialized, return the object
    #    - subscribe to events buildup, process, and teardown
    #
    def __new__(cls):
        if(NodeGUI.__instance is None):
            NodeGUI.__instance = object.__new__(cls);
            ProcMaster().event['buildup'].subscribe(__name__, NodeGUI.open);
            ProcMaster().event['process'].subscribe(__name__, NodeGUI.update);
            ProcMaster().event['teardown'].subscribe(__name__, NodeGUI.close);
        else: 
            pass
        return NodeGUI.__instance;

    #Create the layout of the GUI window and the window itsself
    # @param    params    (str) information on GUI elements to be created - the 2nd half of the 'buildup' payload, delimited by the '|' character
    #
    def build_layout(self, params = None):
        global rx_box, tx_box, report_box, window;
        global HEIGHT, WIDTH, NUM_BARS, NUM_LINES, LINES_HEIGHT, LINES_WIDTH, BARS_HEIGHT, BARS_WIDTH;
        global BG_COLOR, GRAPH_ELEMENT_COLOR, FNT_SIZE, bars, lines;
        
        HEIGHT = 500;
        WIDTH = 800;
        LINES_HEIGHT = BARS_HEIGHT = HEIGHT / 2;
        BG_COLOR = 'black';
        GRAPH_ELEMENT_COLOR = 'white';
        BARS_WIDTH = int(WIDTH / params.split('::')[0].split(':').__len__());
        LINES_WIDTH = int(WIDTH / params.split('::')[1].split(':').__len__());
        FNT_SIZE = 8;
       
        base_padding = 5;
        adj_padding = 5;
        bar_pad = line_pad = (base_padding, base_padding); 

        if(BARS_WIDTH < LINES_WIDTH):
            adj_padding = int(params.split('::')[0].split(':').__len__()/params.split('::')[1].split(':').__len__() * base_padding);
            line_pad = (adj_padding, base_padding);
        elif(BARS_WIDTH > LINES_WIDTH):
            adj_padding = int(params.split('::')[1].split(':').__len__()/params.split('::')[0].split(':').__len__() * base_padding);
            bar_pad = (adj_padding, base_padding);
        
        tabs = [];
        bar_info_layout = [];
        layout = [[],[]];
        graphs_layout = [[],[],[],[],[]];
        tab_layout = [[],[]];
        bars = [];
        lines = [];
        
        #create graphing elements for each one requested 
        for bar_data in params.split('::')[0].split(':'):
            bar_info = bar_data.split(',');
            print(bar_info);
            bar = bar_info[0];
            bar_val = bar_info[1];
            graphs_layout[2].append(gui.Text(str(bar), pad=(int(BARS_WIDTH/2-(str(bar).__len__()*(FNT_SIZE-2))/2 + bar_pad[0]),bar_pad[1])));
            graphs_layout[3].append(gui.Graph((BARS_WIDTH, BARS_HEIGHT), (0,0), (100,100), background_color=BG_COLOR, pad=bar_pad, key=str(bar), tooltip=str(bar)));
            bars.append([str(bar), int(bar_val)]);
            bar_info_layout.append([gui.Text(str(bar) + ':'), gui.Text('0', key=str(bar) + "_info")]);
        for line in params.split('::')[1].split(':'):
            print(line);
            
            lg = line.split(',');
            print(lg);
            lg_name = str(lg[0]);
            lg_xvals  = (int(lg[1]), int(lg[2]));
            lg_yvals  = (int(lg[3]), int(lg[4]));
            lg_scrollable = int(lg[5]);
            lg_bufferval = int(lg[6]);
            lg_size = (LINES_WIDTH, LINES_HEIGHT);
            
            obj_linegraph = LineGraph([lg_name, lg_size, lg_xvals, lg_yvals, lg_scrollable, lg_bufferval], graph_pad=line_pad); 
            graph = obj_linegraph.getGraph();

            graphs_layout[0].append(gui.Text(lg_name, pad=(int(LINES_WIDTH/2-(lg_name.__len__()*(FNT_SIZE-2))/2 + line_pad[0]),line_pad[1])));
            graphs_layout[1].append(graph);
            lines.append([lg_name, obj_linegraph]);
            tab_layout = [[gui.Text('Domain:'), gui.Text('(' + str(lg_xvals[0]) + ',' + str(lg_xvals[1]) + ')', key=str(lg_name + "_DOM")), gui.Text(' | Range:'), gui.Text('(' + str(lg_yvals[0]) + ',' + str(lg_yvals[1]) + ')', key=str(lg_name + "_RNG"))],
                        [gui.Multiline('', disabled=False, autoscroll=True, size = (30,15), key = lg_name + "_tab")]];
            tabs.append([gui.Tab(lg_name, tab_layout)]);
        graphs_layout[4].append(gui.CloseButton('Exit'));
        info_layout = [[gui.Text('Graph data')], [gui.TabGroup(tabs)], [gui.Text('Bar data')]];
        for item in bar_info_layout:
            info_layout.append(item);
        layout = [[gui.Column(graphs_layout), gui.VerticalSeparator(), gui.Column(info_layout)]];
        print('LINES : ' + str(lines));
        title = 'Graphing'

        window = gui.Window(title, element_padding=(0,0), font=('Courier', FNT_SIZE)).Layout(layout);
        window.Finalize();
        #if bar graphs are passed with a non zero initial value, display the value
        for bar in bars:
            if bar[1] != 0:
                print(bar)
                gui.Graph.DrawRectangle(window.FindElement(str(bar[0])), (0,bar[1]), (BARS_WIDTH,0), fill_color=GRAPH_ELEMENT_COLOR)
                print(window.FindElement(str(bar[0]) + "_info"));
                gui.Text.Update(window.FindElement(str(bar[0]) + "_info"), str(bar[1]));

    #Invoked by the event buildup
    # @param    packet    (str) the type of GUI to create, information on specific GUI elements to be created
    # @return             (int) 1 if packet is empty, 0 if not
    #                     - format of packet documented in class header
    #
    #    - fills the lists of line and bar graphs with default values for generic gui
    #    - reads the packet to see if another type of GUI is specified, if so set the type to the value specified and call buildLayout with the parameters received in the packet
    #    - invoke build_layout() to create the GUI
    #    - start the listener thread
    #
    def open(packet = None):
        global queue_data, THREAD_KILL;
        queue_data = Queue();
        THREAD_KILL = 0;
        
        if(packet != None):
            NodeGUI.build_layout(NodeGUI(), packet['payload']);
            listener = threading.Thread(target=NodeGUI().listen, args=(), name='listener0');
            listener.start();
            return 0;
        else: 
            return 1;
        
    #Invoked by event 'teardown'
    #    - kills the listener thread
    #
    def close(self):
        global THREAD_KILL;
        THREAD_KILL = 1;

    #Returns the Window object
    # @return   (Window) window
    #
    def getWindow(self): return window;
